/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.InputStreamReader;


/**
 *
 * @author francisco
 */
public class Dulces {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //lectura de datos por linea de comandos
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try
        {                        
            System.out.print ("Introduce la entrada linea por linea : ");
            String cadena = br.readLine();
            while(!"0 0".equals(cadena))
            {
                String[] split = cadena.split(" ");
                int m = Integer.parseInt(split[0]);
                int n = Integer.parseInt(split[1]);
                int[][] dulces= new int[m][n+1];//columna extra para no hacer un if en el algoritmo

                for (int i = 0; i < m; i++) 
                {
                    String[] fila = br.readLine().split(" ");
                    for (int j = 0; j < n; j++)
                    {
                        dulces[i][j]=Integer.parseInt(fila[j]);

                    }
                    dulces[i][n]=0;// la columna extra son solo ceros
                }
                cadena = br.readLine();
            }
        } catch (Exception e){}
       //.fin lectura

        
    }
    /**
     * busca la mayor cantidad de dulces que puede obtener
     * siguiendo las reglas definidas en el enunciado del problema
     * @param dulces
     * @param m
     * @param n
     * @return 
     */
    public static int dulce(int[][] dulces, int m, int n)
    {
        
        if(m==1 && n==1)
        {
            return dulces[0][0];
        }
        else
        {
           
            int[] maxFila = new int[n];
            //veo cuantos dulces puedo sacar en cada fila
            for (int i = 0; i < m; i++) 
            {
                maxFila[i]=maxDulce(dulces[i], n);
            }
            //ahora veo cuales filas se quedan
            return maxDulce(maxFila, m);
        }
    }
    
    /**
     * busca el maximo de dulces a sacar de una fila
     * @param fila
     * @param n
     * @return 
     */
    public static int maxDulce(int[] fila, int n)
    {
      /** Recursivo
            if(n<=3)
            {
                 if(n==3)
                 {
                     return(max(fila[1],fila[2]+fila[0]));  
                 }
                 else if(n==2)
                 {
                     return max(fila[0],fila[1]);
                 }
                 else
                 {
                    return fila[0];   
                 }
            }

            else
            {
               return max(maxDulce(fila, n-1),fila[n-1]+maxDulce(fila,n-2));
            }
     
       */
        //dinamico
        fila[1]= max(fila[0],fila[1]);

        for (int j = 2; j < n; j++) 
        {

           fila[j] = max(fila[j-1],fila[j]+fila[j-2]); 
        }
        return fila[n-1];
       
    }
    public static int max(int a, int b)
    {
        if (a>b) return a;
        return b;
    }
}
